<?php

namespace App\Controller;

use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\User;
use App\Entity\Event;

class EventsController extends AbstractController
{
    public function getEvents($userId, PaginatorInterface $paginator, Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $user_repo = $em->getRepository(User::class);
        $query = '';

//      Preparo rango de fechas Actual menos 30dias

        $date = new \DateTime();
        $today = $date->format("Y-m-d")." 00:00:00";
        $past = strtotime('-30 day', strtotime($today));
        $past = date("Y-m-d",$past)." 00:00:00";

        if (isset($userId)){
            $dql = 'SELECT e 
                    FROM App\Entity\Event e 
                    WHERE e.user = :user AND e.createdAt BETWEEN :from AND :to';
            $query = $em->createQuery($dql);
            $query->setParameter('user', $userId );
            $query->setParameter('from', $past );
            $query->setParameter('to', $today);

            $user = $user_repo->find($userId);
        } else {
            $dql   = "SELECT e
                      FROM App\Entity\Event e
                      ORDER BY e.id";
            $query = $em->createQuery($dql);

        }

        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            5
        );

        return $this->render('events/index.html.twig', [
            'pagination' => $pagination,
            'user' => isset($user) ? $user : null,
            'dates' => [
                'today' => $today,
                'lastDay' => $past
            ]
        ]);
    }
}
