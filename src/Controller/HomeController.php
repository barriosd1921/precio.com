<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\User;

class HomeController extends AbstractController
{
    public function getUsers(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $user_repo = $em->getRepository(User::class);

        $users = $user_repo->findAll();

        return $this->render('home/index.html.twig', [
            'users' => $users,
        ]);
    }
}
