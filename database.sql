CREATE DATABASE IF NOT EXISTS precio_test;
USE precio_test;

CREATE TABLE users(
    id int(255) auto_increment not null,
    name varchar(50) not null,
    surname varchar(50) not null,
    gender varchar(50) DEFAULT NULL,
    created_at datetime DEFAULT CURRENT_TIMESTAMP,
    updated_at      datetime DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT PK_USERS PRIMARY KEY(id)
)ENGINE=InnoDb;

CREATE TABLE events(
    id int(255) auto_increment not null,
    user_id int(255) not null,
    ip varchar(255) not null,
    agent varchar(255) not null,
    country varchar(2) not null,
    event varchar(6) not null,
    created_at datetime DEFAULT CURRENT_TIMESTAMP,
    updated_at datetime DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT PK_EVENTS PRIMARY KEY(id),
    CONSTRAINT FK_EVENTS_USERS FOREIGN KEY(user_id) REFERENCES users(id)
)ENGINE=InnoDb;

INSERT INTO users (id, name, surname, gender, created_at, updated_at) VALUES
(1, 'David', 'Barrios', 'MALE', '2020-11-21 12:13:31', '2020-11-21 12:13:31'),
(2, 'Estefany', 'Garces', 'FEMALE', '2020-11-21 15:24:21', '2020-11-21 15:24:21'),
(3, 'Lucia', 'Barrios', 'FEMALE', '2020-11-21 15:24:21', '2020-11-21 15:24:21');

INSERT INTO events (id, user_id, ip, agent, country, event, created_at, updated_at) VALUES
(1, 1, '192.168.0.1', 'Chrome', 'CO', 'EVET01', '2020-11-21 12:21:32', '2020-11-21 12:21:32'),
(2, 1, '192.168.0.2', 'Safari', 'CO', 'EVET02', '2020-11-21 12:21:32', '2020-11-21 12:21:32'),
(3, 1, '192.168.0.3', 'Firefox', 'CO', 'EVET03', '2020-11-21 12:21:32', '2020-11-21 12:21:32'),
(4, 1, '192.168.0.4', 'Opera', 'CO', 'EVET04', '2020-11-21 12:21:32', '2020-11-21 12:21:32'),
(5, 2, '192.168.1.1', 'Chrome', 'VE', 'EVET05', '2020-11-21 12:21:32', '2020-11-21 12:21:32'),
(6, 3, '192.168.2.1', 'Chrome', 'US', 'EVET06', '2020-11-21 12:21:32', '2020-11-21 12:21:32'),
(7, 1, '192.168.0.1', 'Chrome', 'US', 'EVET07', '2020-11-21 12:21:32', '2020-11-21 12:21:32'),
(8, 1, '192.168.0.1', 'Firefox', 'US', 'EVET08', '2020-11-21 12:21:32', '2020-11-21 12:21:32');
